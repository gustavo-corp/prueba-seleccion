var charactersDatatable = null
document.addEventListener("turbolinks:before-cache", function() {
  if ($('#characters_datatable_wrapper').length === 1){
    charactersDatatable.destroy();
  }
})

var initCharactersDatatable = function() {
  return charactersDatatable = $('#characters_datatable').DataTable({
    sPaginationType: "full_numbers",
    "bSort": true,
    Processing: true,
    bServerSide: true,
    pageLength: 10,
    sAjaxSource: $('#characters_datatable').data('source'),
      "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [] }
        ],
    "oLanguage": {
      "sLengthMenu": "Display _MENU_ records per page",
      "sZeroRecords": "Nothing found - sorry",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
      "sInfoEmpty": "Showing 0 to 0 of 0 records",
      "sInfoFiltered":   "",
      "sInfoPostFix":    "",
      "sSearch":         "Search : ",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Loading...",

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "oPaginate": {
        "sFirst": "First",
        "sLast": "Last",
        "sNext": "Next",
        "sPrevious": "Previous"
      }
    }
  });
}
initCharactersDatatable();
$(document).on("turbolinks:load", initCharactersDatatable);
$(document).on("page:load", initCharactersDatatable);
