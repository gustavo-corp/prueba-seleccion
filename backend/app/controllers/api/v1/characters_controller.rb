module Api
  module V1
    class CharactersController < V1Controller
      include ApplicationHelper

      def index
        
      end

      def show
        
      end

      def update_characters
        get_data_json('https://api.got.show/api/general/characters')
        render json: { status: :ok }
      end
      
      #def create
      #  @like = Like.new(like_params)
      #  if @like.save
      #    @status = :created
      #  else
      #    @status = :bad_request
      #  end
      #  render status: @status
      #end
      #
      #private
      #def like_params
      #  params.require(:like)
      #        .permit(:comment_id, :user_id)
      #end
    end
  end
end
