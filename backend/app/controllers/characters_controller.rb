class CharactersController < ApplicationController

  include ApplicationHelper
  
  before_action :set_character, only: [:edit, :update,:show, :destroy]
  def index
    @characters = Character.all 
    respond_to do |format|
      format.html
      format.json { render json: CharactersDatatable.new(view_context, @characters) }
    end
  end

  def show 
  end

  def edit
  end
  
  def create
    @character = Character.new(character_params)
    respond_to do |format|
      if @character.save
        format.html { redirect_to characters_path }
      else
        format.html { render :new }
      end
    end
  end
  
  def new
    @character = Character.new
  end
  
  def update
    respond_to do |format|
      if @character.update(character_params)
        format.html { redirect_to character_path }
      else
        format.html { render :edit }
      end
    end
  end


  def destroy
    @character = @character.destroy 
    respond_to do |format|
      if @character.errors.present?
        puts ap @character.errors.messages
        format.html { redirect_to characters_url }
        format.json { head :no_content }
      else
        format.html { redirect_to characters_url}
        format.json { head :no_content }
      end
    end
  end
  
  def acquire_characters
    get_data_json('https://api.got.show/api/general/characters')
    respond_to do |format|
      format.html { redirect_to characters_path }
    end
  end

  private
  def set_character
    @character = Character.find(params[:id])
  end

  def character_params
    params.require(:character)
          .permit(:name, :gender, :slug, :rank, :house)
  end

end
