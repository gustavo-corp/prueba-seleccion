module ApplicationHelper
 
  require 'uri'
  require 'net/http'
  require 'json'
    
  def get_data_json(url)
    url = url.gsub(" ","")
    escaped_address = URI.escape(url) 
    uri = URI.parse(url)

    if uri.kind_of?(URI::HTTP) or uri.kind_of?(URI::HTTPS)
      result = Net::HTTP.get_response(uri)
      if valid_json?(result.body)
        result = JSON.parse(result.body)
      else  
        return false  
      end
      if !result.nil? && !result['success'].nil? && result['success'] == 1 
        result['book'].each do |character|
          Character.create(name: character['name'], gender: character['gender'], slug: character['slug'],
           rank: character['pagerank']['rank'], house: character['house'])
        end
      end
    end
  end

  def valid_json?(string)
    begin
      !!JSON.parse(string)
    rescue JSON::ParserError
      false
    end
  end
end
