Rails.application.routes.draw do
  devise_for :users
  resources :characters do
    collection do 
      post 'acquire_characters'
    end
  end
  devise_scope :user do
	  authenticated :user do
	    root 'characters#index', as: :authenticated_root
	  end
		unauthenticated do
	   root "devise/sessions#new", as: :unauthenticated_root
	  end
  end
  ## API
  namespace :api , defaults: { format: 'json' } do
    namespace :v1 do
      resources :characters do 
        collection do 
          post 'update_characters'
        end
      end
    end
  end
end
