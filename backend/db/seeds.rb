# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
normal_user = User.create(email: "normal@user.com", password: "123123", name: Faker::Name.name )

first_user = User.create(email: "g@g.g", password: "123123", name: Faker::Name.name )
#first_user.add_role(:admin)

second_user = User.create(email: "admin@admin.com", password: "123123", name: Faker::Name.name )
#second_user.add_role(:admin)
i = 0
30.times do 
  Character.create(name: "fake character #{i}" )
  i +=1
end
